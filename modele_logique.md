# Modèle Logique de Données

__logiciel__ (#nomDev, nom, dateLancement, type, sousType, plateforme)

_contraintes_ : nom KEY ; dateLancement NOT NULL ; type NOT NULL ; plateforme NOT NULL ; NOT (type = ‘OS’ AND sousType) ; type = ‘OS’ AND plateforme IN {'smartphone', 'tablette', 'PC', 'serveur'} ; NOT (type = ‘jeu’ AND plateforme) ; NOT (type = ‘application’ AND plateforme) ; type = ‘application’ AND sousType IN {‘texte', 'maths', 'autre'} ; type = ‘jeu’ AND sousType IN {'platesFormes', 'FPS', 'autre'}

---

__version__ (#num, dateDispo, #L→logiciel)

_contraintes_ : dateDispo NOT NULL ; L NOT NULL ; projection(version.L) =
projection(logiciel.nomDev)

---

__problème__ (#id, description, date, v→version(num), L→version(L))

_contraintes_ : description NOT NULL ; date NOT NULL ; v NOT NULL ; L NOT NULL

---

__employé__ (#id, nom, prénom, dateNaissance)

_contraintes_ : nom NOT NULL ; prénom NOT NULL ; dateNaissance NOT NULL

---

__client__ (#numTel, nom, prénom, dateNaissance, numRue, nomRue, codePostal, ville)

_contraintes_ : nom NOT NULL ; prénom NOT NULL ; dateNaissance NOT NULL

---

__achat__ (#c→client , #L→logiciel)

_contraintes_ : projection(achat.c) = projection(client.num)

---

__signalement__ (#c→client , #p→problème)

---

__correctif__ (#id, e→employé , p→problème, date, instructions, patch)

_contraintes_ : e NOT NULL ; p NOT NULL ; patch UNIQUE ; instructions XOR patch ;
correctif.date > problème.date

---

__support__ (#os→logiciel , #programme→logiciel)

_contraintes_ : programme ne référence que des enregistrements tels que type = ‘programme’ ; os ne référence que des enregistrements tels que type = ‘os’ ; restriction(projection(logiciel.type), type = ‘programme’) = projection(support.programme)

---

_contraintes supplémentaires_ : la date d'un correctif doit être postérieure à celle du problème ; la date d'un problème doit être postérieure à celle de la version ; la date d'une version doit être postérieure à celle du logiciel
