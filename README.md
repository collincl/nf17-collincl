## NF17 - Projet 1
#### Sujet Pear

auteur : Clément Collin

Architecture du projet : 

- Le fichier note_de_clarification.md permet d'éclaircir les attentes du client.
- Le fichier uml.png correspond à l'élaboration conceptuelle de la base de données répondant aux besoins énoncés.
- Le fichier modele_logique.md traduit l'UML en relations.
- Le fichier definition_donnees.sql correspond à la réalisation de la base de données.
- Le fichier manipulation_donnees.sql correspond aux requêtes SQL sur la base de données répondant aux demandes du client.