# NOTE DE CLARIFICATION
concernant la mise en place d’une base de données pour la maintenance corrective des logiciels mis au points par l'entreprise Pear.

### Objets de la Base de Données :

+ les _logiciels_ qui ont un nom de développement unique, un nom unique et une date de lancement. Il y a trois types de logiciels : les OS, les applications, qui ont un type, et les jeux, qui ont également un type. Les OS supportent une ou plusieurs plateformes et les applications et jeux un ou plusieurs OS.

+ les _versions_ des logiciels, ayant un numéro de version qui est unique et une date de mise à disponibilité

+ les _problèmes_, qui sont décrits et datés

+ les _correctifs_ apportés à un problème, qui sont datés et se présentant sous la forme d'instructions textuelles ou d'un patch correctif ayant un identifiant unique

+ les _personnes_ identifiées par un nom, un prénom, une date de naissance. Il peut s’agir de clients qui une adresse et un numéro de téléphone ou d’employés qui ont un numéro de poste.

### Associations entre les objets :

+ chaque version est une version d'un logiciel, un logiciel peut avoir plusieurs versions.

+ chaque problème est détecté sur une version particulière d’un logiciel, une version peut engendrer plusieurs problèmes
+ chaque correctif correspond à un problème, un problème peut avoir plusieurs plusieurs correctifs

+ chaque problème est remonté par plusieurs clients

+ chaque correctif sont apportés par un ou plusieurs employés

+ les logiciels sont achetés par des clients à une date donnée

### Requêtes :

+ le nombre de problèmes non corrigés pour chaque version de ses logiciels

+ applications tournant sur tablettes ayant eu plus d'un correctif

+ clients ayant remonté des problèmes sur des logiciels qu'ils ne possèdent pas
